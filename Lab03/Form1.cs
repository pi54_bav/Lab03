﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;


namespace Lab03
{
    public partial class Form1 : Form
    {
        int p1;
        int p2;
        int counter = 0;
        int[,] arr;
        int[,] arr2;
        string filePath;
        string linesToWrite;

        char[] letters = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J' };

        List<int> arrRedCol = new List<int>();
        List<int> arrRedRow = new List<int>();

        public Form1()
        {
            InitializeComponent();
        }

        private void buttonGenerate_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            dataGridView1.Columns.Clear();

            Random rnd = new Random();

            int p1 = Decimal.ToInt32(numericUpDownP1.Value);
            int p2 = Decimal.ToInt32(numericUpDownP2.Value);

            arr = new int[p1, p2];
            arr2 = new int[p1, p2];

            for (int i = 0; i < p1; i++)
            {
                for (int j = 0; j < p2; j++)
                {
                    arr[i, j] = rnd.Next(-9, 9);
                    arr2[i, j] = rnd.Next(-9, 9);
                }
            }

            for (int i = 0; i < p1; i++)
            {
                string column = string.Format("{0}", i + 1);
                dataGridView1.Columns.Add(column, column);
            }

            for (int j = 0; j < p2; j++)
            {
                string[] currentColumn = new string[p1];

                for (int i = 0; i < p1; i++)
                {
                    currentColumn[i] = arr[i, j].ToString() + "; " + arr2[i, j].ToString();
                }

                dataGridView1.Rows.Add(currentColumn);
            }

            linesToWrite = gridToStringFile(dataGridView1);

            for (int i = 0; i < dataGridView1.RowCount; i++) { 
                dataGridView1.Rows[i].HeaderCell.Value = String.Format("{0}", letters[i]);
            }


            for (int i = 0; i < dataGridView1.ColumnCount; i++)
            {
                dataGridView1.Columns[i].HeaderCell.Value = String.Format("{0}", letters[i]);
            }

            foreach (DataGridViewColumn col in dataGridView1.Columns)
            {
                col.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                col.Width = 64;
                col.SortMode = DataGridViewColumnSortMode.NotSortable;
            }

            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                row.Height = 28;
            }

                dataGridView1.ClearSelection();
        }

        private void buttonSolve_Click(object sender, EventArgs e)
        {
            var flag = 0;

            try
            {
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        if (cell.Style.BackColor == Color.PaleVioletRed || cell.Style.BackColor == Color.ForestGreen)
                        {
                            flag = 1;
                        }
                    }
                }

                if (flag == 1) //перевірка на червоні колонки
                {
                    for (int g = 0; g < dataGridView1.RowCount; g++)
                    {
                        for (int p = 0; p < dataGridView1.ColumnCount; p++)
                        {
                            dataGridView1.Rows[g].Cells[p].Style.BackColor = Color.White;
                        }
                    }

                    arrRedCol.Reverse();

                    foreach (int col in arrRedCol)
                    {
                        arr2 = TrimArray(col, arr2);
                        arr = TrimArray(col, arr);
                        dataGridView1.Columns.RemoveAt(col);
                        flag = 0;
                    }

                    linesToWrite += Environment.NewLine;
                    linesToWrite += gridToStringFile(dataGridView1);
                }
                else //якщо домінована колонка видалена/ червоних нема
                {
                    arrRedCol.Clear();

                    for (int i = 0; i < dataGridView1.ColumnCount; i++)
                    {
                        for (int k = 0; k < dataGridView1.ColumnCount; k++)
                        {
                            counter = 0;
                            for (int j = 0; j < dataGridView1.RowCount; j++)
                            {
                                if (arr2[i, j] < arr2[k, j])
                                {
                                    counter++;
                                }
                                else
                                {
                                    counter = 0;
                                    break;
                                }

                                if (counter == dataGridView1.RowCount) //counter == 3
                                {
                                    for (int d = 0; d < dataGridView1.RowCount; d++)
                                    {
                                        dataGridView1.Rows[d].Cells[i].Style.BackColor = Color.PaleVioletRed;
                                        dataGridView1.Rows[d].Cells[k].Style.BackColor = Color.ForestGreen;

                                        if (!arrRedCol.Contains(i))
                                        {
                                            arrRedCol.Add(i);
                                        }
                                    }

                                    counter = 0;
                                }
                            }
                        }
                    }
                }
            }
            catch (System.IndexOutOfRangeException ex)
            {
                Debug.WriteLine(ex.Data);
            }

            dataGridView1.ClearSelection();
        }

        private void buttonDelRow_Click(object sender, EventArgs e)
        {
            var flag = 0;

            try
            {
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        if (cell.Style.BackColor == Color.PaleVioletRed || cell.Style.BackColor == Color.ForestGreen)
                        {
                            flag = 1;
                        }
                    }
                }

                if (flag == 1) //перевірка на червоні рядки
                {
                    for (int g = 0; g < dataGridView1.RowCount; g++)
                    {
                        for (int p = 0; p < dataGridView1.ColumnCount; p++)
                        {
                            dataGridView1.Rows[g].Cells[p].Style.BackColor = Color.White;
                        }
                    }

                    arrRedRow.Reverse();

                    foreach (int row in arrRedRow)
                    {
                        arr2 = TrimArray2(row, arr2);
                        arr = TrimArray2(row, arr);
                        dataGridView1.Rows.RemoveAt(row);
                        flag = 0;
                    }

                    linesToWrite += Environment.NewLine;
                    linesToWrite += gridToStringFile(dataGridView1);
                }
                else //якщо домінований рядок видалений / червоних нема
                {
                    arrRedRow.Clear();

                    for (int i = 0; i < dataGridView1.RowCount; i++)
                    {
                        for (int k = 0; k < dataGridView1.RowCount; k++)
                        {
                            counter = 0;
                            for (int j = 0; j < dataGridView1.ColumnCount; j++)
                            {
                                if (arr[j, i] < arr[j, k])
                                {
                                    counter++;
                                }
                                else
                                {
                                    counter = 0;
                                    break;
                                }

                                if (counter == dataGridView1.ColumnCount) //counter == 3
                                {
                                    for (int d = 0; d < dataGridView1.ColumnCount; d++)
                                    {
                                        dataGridView1.Rows[i].Cells[d].Style.BackColor = Color.PaleVioletRed;
                                        dataGridView1.Rows[k].Cells[d].Style.BackColor = Color.ForestGreen;

                                        if (!arrRedRow.Contains(i))
                                        {
                                            arrRedRow.Add(i);
                                        }
                                    }
                                    counter = 0;
                                }
                            }
                        }
                    }
                }
            }
            catch (System.IndexOutOfRangeException ex)
            {
                Debug.WriteLine(ex.Data);
            }
            dataGridView1.ClearSelection();
        }


        public static int[,] TrimArray(int rowToRemove, int[,] originalArray) //видаляє колонку
        {
            int[,] result = new int[originalArray.GetLength(0) - 1, originalArray.GetLength(1)];

            for (int i = 0, j = 0; i < originalArray.GetLength(0); i++)
            {
                if (i == rowToRemove)
                    continue;
                for (int k = 0, u = 0; k < originalArray.GetLength(1); k++)
                {
                    result[j, u] = originalArray[i, k];
                    u++;
                }
                j++;
            }

            return result;
        }

        public static int[,] TrimArray2(int columnToRemove, int[,] originalArray)//видаляє колонку
        {
            int[,] result = new int[originalArray.GetLength(0), originalArray.GetLength(1) - 1];

            for (int i = 0, j = 0; i < originalArray.GetLength(0); i++)
            {
                for (int k = 0, u = 0; k < originalArray.GetLength(1); k++)
                {
                    if (k == columnToRemove)
                        continue;

                    result[j, u] = originalArray[i, k];
                    u++;
                }
                j++;
            }

            return result;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            dataGridView1.DefaultCellStyle.Font = new Font("Times New Roman", Decimal.ToInt32(numericUpDown1.Value));
        }

        private string gridToStringFile(DataGridView dataGridView)
        {
            string lines = "";

            for (int row = 0; row <dataGridView.RowCount; row++)
            {
                for (int col = 0; col < dataGridView.ColumnCount; col++)
                {
                    lines += dataGridView.Rows[row].Cells[col].Value.ToString()+"\t";
                }

                lines += Environment.NewLine;
            }

            return lines;
        }

        private void toFile(string linesToWrite)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Title = "Save File";
            saveFileDialog1.DefaultExt = "txt";
            saveFileDialog1.ShowDialog();
            
            File.WriteAllText(Path.GetFullPath(saveFileDialog1.FileName), linesToWrite);
        }

        private void buttonSaveFile_Click(object sender, EventArgs e)
        {
            toFile(linesToWrite);
        }
    }
}
